//
//  WebViewViewController.swift
//  VuukleWidget-sample
//
//  Created by Narek Dallakyan on 22.05.22.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {

    private lazy var webConfigs: WKWebViewConfiguration = {
        let configs = WKWebViewConfiguration()
        let thePreferences = WKPreferences()
        thePreferences.javaScriptCanOpenWindowsAutomatically = true
        configs.preferences = thePreferences
        return configs
    }()

    private lazy var webView = WKWebView(frame: view.bounds, configuration: webConfigs)

    override func viewDidLoad() {
        super.viewDidLoad()
        if !webView.isDescendant(of: view) {
            view.addSubview(webView)
        }
    }

    func loadURL(_ url: URL?) {
        guard let url = url else {
            return
        }

        let request = URLRequest(url: url)
        webView.load(request)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let url = URL(string: "about:blank") else { return }
        webView.load(URLRequest(url: url))
    }
}

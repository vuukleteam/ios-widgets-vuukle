//
//  ViewController.swift
//  VuukleWidget-sample
//
//  Created by Narek Dallakyan on 19.03.22.
//

import VuukleWidget
import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var vuukleView1: VuukleView!
    @IBOutlet weak var vuukleView2: VuukleView!

    @IBOutlet weak var versionLabel: UILabel!
    let publisherKey = PublisherKeyPair(privateKey: "bd3a64e4-7e19-46b2-ae4d-a2a0adc72cdf",
                                        publicKey: "664e0b85-5b2c-4881-ba64-3aa9f992d01c")
    private lazy var vuukleManager = VuukleManager(viewController: self, publisherKeyPair: publisherKey)

    let webVC = WebViewViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        versionLabel.text = appVersion

        vuukleManager.addErrorListener = { exception in
            switch exception {
            case .didFailProvisionalNavigation(let error):
                print("Error:", error)
            case .failedToLoadURL(let url, let error):
                print("URL and Error:", url, error)
            @unknown default:
                fatalError()
            }
        }

        vuukleManager.addSSOExceptionsListener = { exception in
            switch exception {
            case .emptyPublisherKeyPair(let message):
                print("Error Message:", message)
            case .authModelEncodingError(let error):
                print("Error", error)
            @unknown default:
                print("Other exceptions", exception)
            }
        }

        vuukleManager.newEvent.talkOfTheTownListener = { [weak self] url in
            print("talkOfTheTown URL - ", url)
            guard let webVC = self?.webVC else { return }
            self?.present(webVC, animated: true)
            webVC.loadURL(url)
        }

        vuukleManager.newEvent.whatsOnYourMindListener = { [weak self] url in
            print("whatsOnYourMind URL - ", url)
            guard let webVC = self?.webVC else { return }
            self?.present(webVC, animated: true)
            webVC.loadURL(url)
        }

        vuukleManager.load(on: vuukleView1, url: URL(string: Environment.shareBar.rawValue)!, backgroundColor: "#FFFF00")
        vuukleManager.load(on: vuukleView2, url: URL(string: Environment.commentsURL.rawValue)!,
                           backgroundColor: "#FFFF00")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    @IBAction func loginSSOTapped(_ sender: Any) {
        vuukleManager.ssoLogin(token: "ewogICJ1c2VybmFtZSI6ICJTYW1wbGUgVXNlciBOYW1lIiwKICAiZW1haWwiOiAibW91c0BlbWFpbC5jb20iLAogICJwdWJsaWNfa2V5IjogImVhZDQxZTQ2LWE1ZmQtMTFlMi1iYzk3LWJjNzY0ZTA0OTJjYyIsCiAgInNpZ25hdHVyZSI6ICIwQzlDMzE0RTM2Qjc4MTc4NkRBNjVGMkNGN0UzMEM3MzUyNjU1MjczNzg2ODMxMzE0QTkzOTRBMjkyNzdDRUI1OURCMzAwNTIwM0E0MkYyREVBOEExNUE0NDYzMDI0M0U1QjRGMTBDMTlBQjBDN0Q3MDNBQzI5RDZDNzhBMDE4MCIKfQ==")
    }
    
    @IBAction func logoutSSOTapped(_ sender: Any) {
        vuukleManager.ssoLogout()
    }

    @IBAction func deleteListenersTapped(_ sender: Any) {
        vuukleManager.newEvent.talkOfTheTownListener = nil
        vuukleManager.newEvent.whatsOnYourMindListener = nil

    }
    
    @IBAction func addNewEventListeners(_ sender: Any) {
        vuukleManager.newEvent.talkOfTheTownListener = { [weak self] url in
            print("talkOfTheTown URL - ", url)
            guard let webVC = self?.webVC else { return }
            self?.present(webVC, animated: true)
            webVC.loadURL(url)
        }

        vuukleManager.newEvent.whatsOnYourMindListener = { [weak self] url in
            print("whatsOnYourMind URL - ", url)
            guard let webVC = self?.webVC else { return }
            self?.present(webVC, animated: true)
            webVC.loadURL(url)
        }
    }
}


var appVersion: String {
    //versionNumber
    //  let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") ?? "1.0"

    //buildNumber
    let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") ?? "1.0"

    return "Version: \(buildNumber)"
}
